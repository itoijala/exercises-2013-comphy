#include <cmath>
#include <cstdio>
#include <functional>

using namespace std;

double simpson(function<double(double)> f, double a, double b, unsigned int N)
{
	N *= 2;
	double h = (b - a) / N;
	double sum = 0;
	for (unsigned int i = 2; i < N; i += 2)
	{
		sum += 2 * f(a + h * i);
	}
	for (unsigned int i = 1; i < N; i += 2)
	{
		sum += 4 * f(a + h * i);
	}
	sum += f(a) + f(b);
	sum *= h / 3;
	return sum;
}

double rectangle(function<double(double)> f, double a, double b, unsigned int N)
{
	double h = (b - a) / N;
	double sum = 0;
	for (unsigned int i = 0; i < N; i++)
	{
		sum += f(a + h * (i + 0.5));
	}
	sum *= h;
	return sum;
}

void a()
{
	function<double(double)> f1 = [](double t) -> double {return exp(t) / t;};

	double epsilon = 1e-6;
	function<double(double)> f2 = [epsilon](double s) -> double {return (exp(epsilon * s) - 1) / s;};

	unsigned int N = 1000;
	double homer = simpson(f1, -1, -epsilon, N);
	double bart = simpson(f1, epsilon, 1, N);
	double lisa = rectangle(f2, -1, 1, N);
	double I1 = homer + bart + lisa;

	printf("I1: %.10f\n", I1);
}

void b()
{
	function<double(double)> f = [](double x) -> double {return 2 * exp(- x * x);};
	double I2_analytisch = sqrt(M_PI);

	unsigned int N = 1000;
	double b = 100;
	double I2 = simpson(f, 0, b, N);
	do
	{
		N *= 2;
		b *= 2;
		I2 = simpson(f, 0, b, N);
	}
	while (abs(I2_analytisch - I2) / I2_analytisch >= 1e-5);

	printf("I2: %.10f\n", I2);
}

int main()
{
	a();
	b();
}
