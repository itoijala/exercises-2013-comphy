import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

qx, qy, I = np.loadtxt("2c.csv", unpack=True)
qx = qx[:61]
qy = qx
I = np.reshape(I, (61, 61))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
im = ax.imshow(I, origin='lower')
ax.xaxis.set_major_formatter(mpl.ticker.FuncFormatter(lambda x, y: int(x - 30)))
ax.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(lambda x, y: int(x - 30)))
ax.set_xlabel("$q_x$")
ax.set_ylabel("$q_y$")
cb = fig.colorbar(im, ax=ax)
cb.set_label("$I(q_x, q_y)$")
fig.savefig('2c.pdf')
