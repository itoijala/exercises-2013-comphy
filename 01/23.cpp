#include <iostream>
#include <cmath>

using namespace std;

double trapezoidal(double (*f)(double), double a, double b, unsigned int N)
{
	double h = (b - a) / N;
	double sum = 0;
	for (unsigned int i = 1; i < N; i++)
	{
		sum += 2 * f(a + h * i); 
	}
	sum += f(a) + f(b);
	sum *= h / 2;
	return sum;
}

double simpson(double (*f)(double), double a, double b, unsigned int N)
{
	N *= 2;
	double h = (b - a) / N;
	double sum = 0;
	for (unsigned int i = 2; i < N; i += 2)
	{
		sum += 2 * f(a + h * i); 
	}
	for (unsigned int i = 1; i < N; i += 2)
	{
		sum += 4 * f(a + h * i); 
	}
	sum += f(a) + f(b);
	sum *= h / 3;
	return sum;
}

double rectangle(double (*f)(double), double a, double b, unsigned int N)
{
	double h = (b - a) / N;
	double sum = 0;
	for (unsigned int i = 0; i < N; i++)
	{
		sum += f(a + h * (i + 0.5)); 
	}
	sum *= h;
	return sum;
}

void ex3work(double (*s)(double (*)(double), double, double, unsigned int), double (*f)(double), double a, double b, unsigned int N, double epsilon)
{
	double old;
	double current = s(f, a, b, N);
	do
	{
		old = current;
		N *= 2;
		current = s(f, a, b, N);

	}
	while (fabs(old - current) / old >= epsilon);
	cout << "N:       " << N << endl;
	cout << "current: " << current << endl;
}

void ex3()
{
	double (*f1)(double) = [](double x) -> double {return exp(-x) / x;};
	double (*f2)(double) = [](double x) -> double {return x * sin(1 / x);};
	cout << "Trapezoidal I1" << endl;
	ex3work(trapezoidal, f1, 1, 100, 10, 1e-4);
	cout << "Simpson I1" << endl;
	ex3work(simpson, f1, 1, 100, 10, 1e-4);
	cout << "Rectangle I1" << endl;
	ex3work(rectangle, f1, 1, 100, 10, 1e-4);
	cout << "Trapezoidal I2" << endl;
	ex3work(trapezoidal, f2, 1e-10, 1, 10, 1e-4);
	cout << "Simpson I2" << endl;
	ex3work(simpson, f2, 1e-10, 1, 10, 1e-4);
	cout << "Rectangle I2" << endl;
	ex3work(rectangle, f2, 1e-10, 1, 10, 1e-4);
}

int main()
{
	ex3();
	return 0;
}
