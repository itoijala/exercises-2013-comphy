#include <cstdio>

#include "1.h"

using namespace std;

void ex2()
{
	auto F = [](double *r, double *F)
	{
		F[0] = -r[0];
	};
	int N = 10000;
	double t1 = 100;
	double **rv = new double*[N + 1];
	for (int i = 0; i <= N; i++)
	{
		rv[i] = new double[2];
	}
	rv[0][0] = 1;
	rv[0][1] = 0;
	newton(F, N, 1, t1, rv);

	FILE *file = fopen("2.csv", "w");
	for (int i = 0; i <= N; i++)
	{
		double r = rv[i][0];
		double v = rv[i][1];
		fprintf(file, "%.15f %.15f %.15f %.15f\n", t1 / N * i, r, v, 0.5 * (r * r + v * v));
	}
	fclose(file);

	for (int i = 0; i <= N; i++)
	{
		delete[] rv[i];
	}
	delete[] rv;
}

int main()
{
	ex2();
	return 0;
}

