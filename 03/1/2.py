import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt;

t, r, v, E = np.loadtxt("2.csv", unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, r, "b-", label="$r(t)$")
ax.plot(t, v, "r-", label="$v(t)$")
ax.plot(t, E, "g-", label="$E(t)$")
ax.set_xlabel("$t$")
ax.legend(loc="best")
fig.savefig("2.pdf")
