import numpy as np
import matplotlib as mlp
import matplotlib.pyplot as plt

for alpha in [0.9, 1, 1.1]:
    t, rx, ry, rz, vx, vy, vz = np.loadtxt("3a-{:.1f}.csv".format(alpha), unpack=True)
    E_kin, E_pot, E_ges, L_ges, LeRu_x, LeRu_y, LeRu_z = np.loadtxt("3c-{:.1f}.csv".format(alpha), unpack=True)

    plt.subplot(2,1,1)
    plt.plot(rx, ry, 'r-')
    plt.xlim(-0.5, 1.2)
    plt.ylim(-1.0, 1.0)
    plt.grid(True)
    plt.subplot(2,1,2)
    plt.plot(t, LeRu_x, 'b-', label="$x$")
    plt.plot(t, LeRu_y, 'r-', label="$y$")
    plt.plot(t, LeRu_z, 'g--', label="$z$")
    plt.xlabel("$t$")
    plt.legend(loc="best")
    plt.savefig("3a-{:.1f}.pdf".format(alpha))
    plt.clf()

    plt.plot(t, E_kin, 'r-', label="kinetische Energie");
    plt.plot(t, E_pot, 'b-', label="potentielle Energie");
    plt.plot(t, E_ges, 'g-', label="Gesamtenergie");
    plt.xlabel("$t$")
    plt.legend(loc="best")
    plt.savefig("3b-{:.1f}.pdf".format(alpha))
    plt.clf()

