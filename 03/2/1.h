#include <functional>

#ifndef H_1
#define H1

void runge_kutta(std::function<void(double x, double *y, double *yd)> f, int D, int N, double x_0, double x_n, double **y);

void fwork(std::function<void(double *rv, double *rvD)> F, int D, int N, double x_0, double x_n, double **y);

#endif
