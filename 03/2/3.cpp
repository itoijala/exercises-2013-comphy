#include <cstdio>
#include <cmath>
#include "1.h"

void cross_product(double *a, double *b, double *result)
{
    result[0] = a[1] * b[2] - a[2] * b[1];
    result[1] = a[2] * b[0] - a[0] * b[2];
    result[2] = a[0] * b[1] - a[1] * b[0];
}

void setInitialConditions(double** ys, double rx, double ry, double rz, double vx, double vy, double vz)
{
    ys[0][0] = rx;
    ys[0][1] = ry;
    ys[0][2] = rz;
    ys[0][3] = vx;
    ys[0][4] = vy;
    ys[0][5] = vz;
}

void ex3(double alpha)
{
    auto F = [alpha](double *rv, double *F)
    {
        double r = sqrt(rv[0] * rv[0] + rv[1] * rv[1] + rv[2] * rv[2]);
        F[0] = - alpha * rv[0] / pow(r, alpha + 2);
        F[1] = - alpha * rv[1] / pow(r, alpha + 2);
        F[2] = - alpha * rv[2] / pow(r, alpha + 2);
    };

    int N = 50000;
    double **ys = new double*[N+1];
    double x_0 = 0;
    double x_n = 4;
    double h = (x_n - x_0) / N;
    for (int i = 0; i <= N; i++)
    {
        ys[i] = new double[6];
    }

	setInitialConditions(ys, 1, 0, 0, 0, 0.5, 0);
    fwork(F, 3, N, x_0, x_n, ys);

	char *name = new char[11];
	sprintf(name, "3a-%.1f.csv", alpha);
    FILE *outfile1 = fopen(name, "w");
    delete[] name;
    for (int i = 0; i <= N; i++)
    {
        fprintf(outfile1, "%.10f %.10f %.10f %.10f %.10f %.10f %.10f\n", x_0 + i * h, ys[i][0], ys[i][1], ys[i][2], ys[i][3], ys[i][4], ys[i][5]);
    }
    fclose(outfile1); 

    double *E_kin = new double[N + 1];
    double *E_pot = new double[N + 1];
    double *E_ges = new double[N + 1];
    double **L = new double*[N + 1];
    double **LeRu = new double*[N + 1];

	name = new char[11];
	sprintf(name, "3c-%.1f.csv", alpha);
    FILE *outfile2 = fopen(name, "w");
    delete[] name;
    for (int i = 0; i <= N; i++)
    {
       double r = sqrt(ys[i][0] * ys[i][0] + ys[i][1] * ys[i][1] + ys[i][2] * ys[i][2]);    
       E_kin[i] = 0.5 * (ys[i][3] * ys[i][3] + ys[i][4] * ys[i][4] + ys[i][5] * ys[i][5]);
       E_pot[i] = - 1.0 / pow(r, alpha);
       E_ges[i] = E_kin[i] + E_pot[i];
       
       L[i] = new double[3];
       LeRu[i] = new double[3];

       cross_product(ys[i], ys[i]+3, L[i]);
       double L_ges = sqrt(L[i][0] * L[i][0] + L[i][1] * L[i][1] + L[i][2] * L[i][2]);

       cross_product(ys[i] + 3, L[i], LeRu[i]);
       LeRu[i][0] -= ys[i][0] / r;
       LeRu[i][1] -= ys[i][1] / r;
       LeRu[i][2] -= ys[i][2] / r;

       fprintf(outfile2, "%.10f %.10f %.10f %.10f %.10f %.10f %.10f\n", E_kin[i], E_pot[i], E_ges[i], L_ges, LeRu[i][0], LeRu[i][1], LeRu[i][2]);
    }
    fclose(outfile2);
    for (int i = 0; i <=N; i++)
    {
        delete[] L[i];
        delete[] LeRu[i];
    }
    delete[] E_kin;
    delete[] E_pot;
    delete[] E_ges;
    delete[] L;
    delete[] LeRu;
}

void ex3d()
{
    auto F = [](double *rv, double *F)
    {
        double r = sqrt(rv[0] * rv[0] + rv[1] * rv[1] + rv[2] * rv[2]);
        F[0] = - rv[0] / (r * r * r);
        F[1] = - rv[1] / (r * r * r);
        F[2] = - rv[2] / (r * r * r);
    };

    int N = 100000;
    double **ys = new double*[N+1];
    double **zs = new double*[N+1];
    double x_0 = 0;
    double x_n = 7;
    for (int i = 0; i <= N; i++)
    {
        ys[i] = new double[6];
    }
    for (int i = 0; i <= N; i++)
    {
        zs[i] = new double[6];
    }
    
	setInitialConditions(ys, 1, 0, 0, 0, 0.5, 0);
    fwork(F, 3, N, x_0, x_n, ys);
    
    setInitialConditions(zs, ys[N][0], ys[N][1], ys[N][2], -ys[N][3], -ys[N][4], -ys[N][5]);
    fwork(F, 3, N, x_0, x_n, zs);
    
    printf("Am Ausgangspunkt:\t%.10f %.10f %.10f\n", ys[0][0], ys[0][1], ys[0][2]);
    printf("Nach Zeitumkehr:\t%.10f %.10f %.10f\n", zs[N][0], zs[N][1], zs[N][2]);
}

int main()
{
    ex3(0.9);
    ex3(1.0);
    ex3(1.1);
    ex3d();
    return 0;
}
