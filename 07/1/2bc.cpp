#include <cmath>
#include <functional>

using namespace std;

double bisection(function<double(double)> f, double x_left, double x_right, double epsilon)
{
	double f_left = f(x_left);
	while (x_right - x_left > epsilon)
	{
		double z = 0.5 * (x_right + x_left);
		double f_z = f(z);
		if (f_z == 0)
		{
			x_left = z;
			x_right = z;
		}	
		else if (f_z * f_left < 0)
		{
			x_right = z;
		}
		else
		{
			x_left = z;
		}
		f_left = f(x_left);
	}
	return 0.5 * (x_right + x_left);
}

function<double(double)> g_factory(function<double(double, double)> f, size_t n)
{
	return [f, n](double r)
	{
		double x = 0.5;
		for (size_t i = 0; i < (size_t) pow(2, n); i++)
		{
			x = f(r, x);
		}
		return 0.5 - x;
	};
}

int main()
{
	double epsilon = 1e-7;

	auto g = g_factory([](double r, double x) {return r * x * (1 - x);}, 3); 
	double R0 = bisection(g, 1.9, 2.1, epsilon);
	double R1 = bisection(g, 3.2, 3.3, epsilon);
	double R2 = bisection(g, 3.48, 3.52, epsilon);
	double R3 = bisection(g, 3.55, 3.58, epsilon);
	double delta = (R2 - R1) / (R3 - R2);

	printf("%.7f, %.7f, %.7f, %.7f\n%.7f\n", R0, R1, R2, R3, delta);

	return 0;
}
