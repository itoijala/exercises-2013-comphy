#include <iostream>
#include <cmath>
#include <string>
#include <functional>

using namespace std;

double bisection(double x_0, double y_0, function<double(double)> f, double epsilon)
{
	double x = x_0;
	double y = y_0;
	double z = (x + y) / 2;

	while(y - x > epsilon)
	{
		z = (x + y) / 2;
		if (f(z) * f(x) < 0)
		{
			y = z;
		}
		else
		{
			x = z;
		}
	}
	return z;
}

void work()
{
    auto g = [](double n, double r)
    {
        auto f = [r](double x){return r * x * (1 - x);};
        double x = 1.0 / 2;
        for (int i = 0; i < pow(2, n); i++)
        {
            x = f(x);
        }
        return 1.0 / 2 - x;
    };

    double r_0   = 0;
    double r_inf = 3.5699;
    int N = 1000;
    double h = (r_inf - r_0) / N;

    FILE *file = fopen("2a.txt", "w");

    for (int i = 0; i < N; i++)
    {
        double r = r_0 + i * h;
        fprintf(file, "%.3f ", r);
        for (int n = 0; n < 4; n++)
        {
            fprintf(file, "%.15f ", g(n, r));
        }
        fprintf(file, "\n");
    }

    double eps = 1e-5;
    double R1 = bisection(0, r_inf, bind(g, 0, std::placeholders::_1), eps);
    double R2 = bisection(R1+eps, r_inf, bind(g, 1, std::placeholders::_1), eps);
    double R3 = bisection(R2+eps, r_inf, bind(g, 2, std::placeholders::_1), eps);
    double R4 = bisection(R3+eps, r_inf, bind(g, 3, std::placeholders::_1), eps);

    double feigenbaum = (R3 - R2) / (R4 -R3);

    cout << "Nullstellen:" << endl << "R1 = " << R1 << endl << "R2 = " << R2 << endl << "R3 = " << R3 << endl << "R4 = " << R4 << endl;
    cout << endl << "Approximation der Feigenbaumkonstante" << endl << "δ = " << feigenbaum << endl;
}

int main()
{
    work();
    return 0;
}
