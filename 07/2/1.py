import matplotlib.pyplot as plt
import numpy as np;

x = np.loadtxt("a_logistic.csv")
yp = np.loadtxt("ap_cubic.csv")
ym = np.loadtxt("am_cubic.csv")


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(x[:,0], x[:,1:], "r,")
fig.savefig('logistic.png')

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)
ax1.plot(yp[:,0], yp[:,1:], "r,")
ax1.set_ylabel('$x_n$, $x_0 = 0.5$')
ax2 = fig.add_subplot(2, 1, 2, sharex=ax1)
ax2.plot(ym[:,0], ym[:,1:], "r,")
ax2.set_ylabel('$x_n$,  $x_0 = -0.5$')
fig.subplots_adjust(hspace=0.15)
fig.savefig('cubic.png')
