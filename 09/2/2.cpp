#include <iostream>
#include <functional>
#include <cmath>
#include <random>

using namespace std;

double elMonteCarlo(size_t N, double a, double b, function<double(double, double)> f)
{
    mt19937_64 rand(43);
    uniform_real_distribution<double> uniform(0, 1);

    double sum = 0;
    for (size_t i = 0; i < N; i++)
    {
    	double x = uniform(rand) * a;
    	double y = uniform(rand) * b;
    	if (pow(x / a, 2) + pow(y / b, 2) < 1)
    	{
    		sum += f(x, y);
    	}
    }
    return 4.0 * a * b * sum / N;
}

void calcPiErr()
{
	FILE* outfile = fopen("pi_err.txt", "w");
	auto f = [](double x, double y){return 1;};
	for (size_t k = 1; k <= 6; k++)
	{
		fprintf(outfile, "%.15f\n", abs(elMonteCarlo(pow(10, k), 1, 1, f) - M_PI));
	}
    fclose(outfile);
}

void elArea()
{
    FILE* outfile = fopen("el_area.txt", "w");
	auto f = [](double x, double y){return 1;};
    for (size_t a = 1; a < 20; a++)
    {
        fprintf(outfile, "%.15f\n", elMonteCarlo(100000, a, 2, f));
    }
    fclose(outfile);
}

void circArea()
{
    FILE* outfile = fopen("circ_area.txt", "w");
	auto f = [](double x, double y){return 1;};
    mt19937_64 rand(43);
    uniform_real_distribution<double> uniform(0, 1);

    for (size_t k = 0; k < 1000; k++)
    {
        double sum = 0;
        for (size_t i = 0; i < 100000; i++)
        {
        	double x = uniform(rand);
        	double y = uniform(rand);
        	if (x * x + y * y < 1)
        	{
        		sum ++;
        	}
        }
        fprintf(outfile, "%.15f\n", 4 * sum / 100000);
    }
    fclose(outfile);
}

int main()
{
	calcPiErr();
    elArea();
    circArea();

	auto f = [](double x, double y){return exp(-x*x);};
	cout << elMonteCarlo(10000000, sqrt(2), 1, f) << endl; //2.99388
	return 1;
}
