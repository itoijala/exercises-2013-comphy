import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys

t = int(sys.argv[1])

p = np.loadtxt("1c-{}.csv".format(t), unpack=True)

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
im = ax1.imshow(p, interpolation="nearest", origin="lower")
fig.savefig("1c-{}.pdf".format(t))
