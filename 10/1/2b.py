import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

for T in [1, 2.25, 3]:
    for a0 in ["ones", "randoms"]:
        t, E, M = np.loadtxt("2b-{:.2f}-{}.csv".format(T, a0), unpack=True)
        fig = plt.figure()
        ax1 = fig.add_subplot(2, 1, 1)
        ax1.plot(t, E)
        plt.setp(ax1.get_xmajorticklabels(), visible=False)
        ax1.set_ylabel('$E$')
        ax2 = fig.add_subplot(2, 1, 2)
        ax2.plot(t, M)
        ax2.set_xlabel('$t$')
        ax2.set_ylabel('$M$')
        fig.savefig("2b-{:.2f}-{}.pdf".format(T, a0))
