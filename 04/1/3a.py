import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
r, v = np.loadtxt("3a.csv", unpack=True)
ax.plot(r / np.pi, v)
ax.set_xlabel(r"$\theta/\pi$")
ax.set_ylabel(r"$\dot\theta$")
fig.savefig("3a.pdf")
