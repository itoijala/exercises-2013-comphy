#include <cmath>
#include <cstdio>
#include <functional>
#include <iostream>

#include "newton.h"

using namespace std;

void ex1a()
{
	auto homolinear = [](double t, double *theta, double *F) {F[0] = - theta[0];};
	double t1 = 2 * M_PI * 1000;
	FILE *file[] = {fopen("1a0.csv", "w"), fopen("1a1.csv", "w"), fopen("1a2.csv", "w"), fopen("1a3.csv", "w")};
	size_t N[] = {5000, 9000, 13000, 170000};
	for (size_t i = 0; i < 4; i++)
	{
		double **rv = new double*[N[i] + 1];
		for (size_t j = 0; j <= N[i]; j++)
		{
			rv[j] = new double[2];
		}
		rv[0][0] = 1;
		rv[0][1] = 0;
		newton(homolinear, N[i], 1, t1, rv);
		for (size_t j = 0; j <= N[i]; j++)
		{
			double r = rv[j][0];
			double v = rv[j][1];
			fprintf(file[i], "%.15f %.15f\n", t1 / N[i] * j, 0.5 * (r * r + v * v));
			delete[] rv[j];
		}
		fclose(file[i]);
		delete[] rv;
	}
}

void ex1b()
{
	double A = 1.5;
	double omega = 2./ 3;
	double Q[] = {0.25, 0.5, 1};

	size_t N = 1000;
	double t1 = 100;
	double ***rv = new double**[3];
	for (size_t i = 0; i < 3; i++)
	{
		double Q_i = Q[i];
		auto inhomolinear = [A, omega, Q_i](double t, double *theta, double *F) {F[0] = -theta[1] / Q_i - theta[0] + A * cos(omega * t);};
		rv[i] = new double*[N + 1];
		for (size_t j = 0; j <= N; j++)
		{
			rv[i][j] = new double[2];
		}
		rv[i][0][0] = 0;
		rv[i][0][1] = 0;
		newton(inhomolinear, N, 1, t1, rv[i]);
	}

	FILE *file[] = {fopen("1bQ0.csv", "w"), fopen("1bQ1.csv", "w"), fopen("1bQ2.csv", "w")};
	for (size_t i = 0; i < 3; i++)
	{
		for (size_t j = 0; j <= N; j++)
		{
			double r = rv[i][j][0];
			double v = rv[i][j][1];
			fprintf(file[i], "%.15f %.15f %.15f\n", t1 / N * j, r, v);
			delete[] rv[i][j];
		}
		delete[] rv[i];
		fclose(file[i]);
	}
	delete[] rv;
}

void ex2a()
{
	auto homonichtlinear = [](double t, double *theta, double *F) {F[0] = - sin(theta[0]);};
	double t1 = 2 * M_PI * 1000;
	FILE *file[] = {fopen("2a0.csv", "w"), fopen("2a1.csv", "w"), fopen("2a2.csv", "w"), fopen("2a3.csv", "w")};
	size_t N[] = {5000, 9000, 13000, 170000};
	for (size_t i = 0; i < 4; i++)
	{
		double **rv = new double*[N[i] + 1];
		for (size_t j = 0; j <= N[i]; j++)
		{
			rv[j] = new double[2];
		}
		rv[0][0] = 1;
		rv[0][1] = 0;
		newton(homonichtlinear, N[i], 1, t1, rv);
		for (size_t j = 0; j <= N[i]; j++)
		{
			double r = rv[j][0];
			double v = rv[j][1];
			fprintf(file[i], "%.15f %.15f\n", t1 / N[i] * j, 1 - cos(r) + 0.5 * v * v);
			delete[] rv[j];
		}
		fclose(file[i]);
		delete[] rv;
	}
}

void ex2b()
{
	double A = 1.5;
	double omega = 2./ 3;
	double Q[] = {0.5, 1, 1.2, 1.3, 1.4};

	size_t N = 1000;
	double t1 = 100;
	double ***rv = new double**[5];
	for (size_t i = 0; i < 5; i++)
	{
		double Q_i = Q[i];
		auto inhomonichtlinear = [A, omega, Q_i](double t, double *theta, double *F) {F[0] = -theta[1] / Q_i - sin(theta[0]) + A * cos(omega * t);};
		rv[i] = new double*[N + 1];
		for (size_t j = 0; j <= N; j++)
		{
			rv[i][j] = new double[2];
		}
		rv[i][0][0] = 0;
		rv[i][0][1] = 0;
		newton(inhomonichtlinear, N, 1, t1, rv[i]);
	}

	FILE *file[] = {fopen("2bQ0.csv", "w"), fopen("2bQ1.csv", "w"), fopen("2bQ2.csv", "w"), fopen("2bQ3.csv", "w"), fopen("2bQ4.csv", "w")};
	for (size_t i = 0; i < 5; i++)
	{
		for (size_t j = 0; j <= N; j++)
		{
			double r = rv[i][j][0];
			double v = rv[i][j][1];
			fprintf(file[i], "%.15f %.15f %.15f\n", t1 / N * j, r, v);
			delete[] rv[i][j];
		}
		delete[] rv[i];
		fclose(file[i]);
	}
	delete[] rv;
}

int main()
{
	ex1a();
	ex1b();
	ex2a();
	ex2b();
	return 0;
}
