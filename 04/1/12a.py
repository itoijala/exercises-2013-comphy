import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
N = [5000, 9000, 13000, 170000]
for i in range(4):
    t, E = np.loadtxt("1a{}.csv".format(i), unpack=True)
    ax.plot(t / (2 * np.pi),  E, label="$N = {}$".format(N[i]))
ax.set_xlabel(r"$t / T$")
ax.set_ylabel(r"$E$")
ax.set_ylim(0, 0.6)
ax.legend(loc="best")
fig.savefig("1a.pdf")

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
for i in range(4):
    t, E = np.loadtxt("2a{}.csv".format(i), unpack=True)
    ax.plot(t,  E, label="$N = {}$".format(N[i]))
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$E$")
ax.set_ylim(0, 0.6)
ax.set_xlim(0, 2 * np.pi * 1000)
ax.legend(loc="best")
fig.savefig("2a.pdf")
