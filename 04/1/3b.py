import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


dtheta = [-5, -3, 0, 3, 5]

for i in range(5):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    Q, v = np.loadtxt("3b{}.csv".format(i), unpack=True)
    ax.plot(Q, v, "r.", label=r"$\dot\theta(0) = {}$".format(dtheta[i]))
    ax.set_xlabel(r"$Q$")
    ax.set_ylabel(r"$\dot\theta(t \gg 1)$")
    ax.legend(loc="best")
    fig.savefig("3b{}.pdf".format(i))
