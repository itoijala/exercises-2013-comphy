import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
q = [0.25, 0.5, 1]
for i in range(3):
    t, r, v = np.loadtxt("1bQ{}.csv".format(i), unpack=True)
    ax.plot(r / np.pi, v, label="$Q = {:.2f}$".format(q[i]))
ax.set_xlabel(r"$\theta/\pi$")
ax.set_ylabel(r"$\dot\theta$")
ax.legend(loc="best")
fig.savefig("1b.pdf")

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
q = [0.5, 1, 1.2, 1.3, 1.4]
for i in range(5):
    t, r, v = np.loadtxt("2bQ{}.csv".format(i), unpack=True)
    ax.plot(r / np.pi, v, label="$Q = {:.1f}$".format(q[i]))
ax.set_xlabel(r"$\theta/\pi$")
ax.set_ylabel(r"$\dot\theta$")
ax.legend(loc="upper left")
fig.savefig("2b.pdf")
