import matplotlib.pyplot as plt
import numpy as np

t, check, x, v = np.loadtxt("3a.csv", unpack=True)


check = np.asarray(check, 'bool')
x = x[check]
v = v[check]

plt.plot(x, v, 'b.')
plt.xlabel(r"$\theta$")
plt.ylabel(r"$\dot{\theta}$")
plt.savefig("3a.pdf")
plt.clf()

for j in [-5, -3, 0, 1, 2]:
    qs, thetads = np.loadtxt("3b-{}.csv".format(j), unpack=True)
    plt.xlabel("$Q$")
    plt.ylabel(r"$\dot{\theta}(t_n)$")
    plt.plot(qs, thetads, 'r.')
    plt.savefig("3b-{}.pdf".format(j))
