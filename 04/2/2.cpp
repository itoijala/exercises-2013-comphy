#include <cmath>
#include <cstdio>
#include <functional>

#include "rungekutta.h"

using namespace std;

void a()
{
    auto f = [](double x, double *y, double *yd)
    {
        yd[0] = y[1];
        yd[1] = -sin(y[0]);
    };

    unsigned int N = 1000;
    double x_0 = 0;
    double x_n = 5;
    double h = (x_n - x_0) / N;
    double **ys = new double *[N+1];
    for (size_t i = 0; i <= N; i++)
    {
        ys[i] = new double[2];
    }

    ys[0][0] = 1;
    ys[0][1] = 0;

    runge_kutta(f, 2, N, x_0, x_n, ys); 

    FILE *outfile = fopen("2a.csv", "w");
    for (size_t i = 0; i <= N; i++)
    {
        double E_pot = 1 - cos(ys[i][0]);
        double E_kin = 0.5 * ys[i][1] * ys[i][1];
        fprintf(outfile, "%.10f %.10f %.10f %.10f %.10f %.10f\n", x_0 + h * i, ys[i][0], ys[i][1], E_kin, E_pot, E_kin + E_pot);
    }
    fclose(outfile);
    for (size_t i = 0; i <= N; i++)
    {
        delete[] ys[i];
    }
    delete[] ys;
}

void b(double A, double w, double Q)
{
    auto f = [A, w, Q](double x, double *y, double *yd)
    {
        yd[0] = y[1];
        yd[1] = - y[1] / Q - sin(y[0]) + A * cos(w * x);
    };

    unsigned int N = 5000;
    double x_0 = 0;
    double x_n = 30;
    double h = (x_n - x_0) / N;
    double **ys = new double *[N+1];
    for (size_t i = 0; i <= N; i++)
    {
        ys[i] = new double[2];
    }

    ys[0][0] = 0;
    ys[0][1] = 0;

    runge_kutta(f, 2, N, x_0, x_n, ys); 

    char *name = new char[11];
    sprintf(name, "2b-%.1f.csv", Q);
    FILE *outfile = fopen(name, "w");
    delete[] name;
    for (size_t i = 0; i <= N; i++)
    {
        fprintf(outfile, "%.10f %.10f %.10f\n", x_0 + h * i, ys[i][0], ys[i][1]);
    }
    fclose(outfile);
    for (size_t i = 0; i <= N; i++)
    {
        delete[] ys[i];
    }
    delete[] ys;
}

int main()
{
    a();
    b(1.5, 0.666666666, 0.5);
    b(1.5, 0.666666666, 1.0);
    b(1.5, 0.666666666, 1.2);
    b(1.5, 0.666666666, 1.3);
    b(1.5, 0.666666666, 1.4);
}

