import sys
import numpy as np
import pandas
import matplotlib as mpl
import matplotlib.pylab as plt
import sympy
from sympy import var, acos, sin, diff
from sympy.utilities.lambdify import lambdify

L = int(sys.argv[1].split("-")[1])
T = float(sys.argv[1].split("-")[2][:-4])
bins = 999

r = var('r')
phi = acos(L / (2 * r))
A = 4 * (phi * r**2 - L / 2 * r * sin(phi))
f = lambdify(r, 1 - sympy.Heaviside(r - L / 2) * diff(A, r) / (2 * np.pi * r))

b, g = pandas.read_table(sys.argv[1], sep=' ').values.T

correction = np.array([f(b * L / bins) if b / bins > 0.5 else 1 for b in range(bins)])
correction[correction <= 0] = 1

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_title("$L = {:d}$, $T = {:.2f}$".format(L, T))
ax.plot(b / bins, g, "b-")
#ax.plot(b / bins, g / correction, "b-")
ax.set_xlim(0, 0.5)
ax.set_xlabel("$r / L$")
ax.set_ylabel("$g(r)$")
fig.savefig(sys.argv[2])
