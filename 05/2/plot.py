
from numpy import *

x, y, vx, vy = loadtxt('md.txt').T

import matplotlib.pyplot as plt

#print(mean(vx[-16:]), mean(vy[-16:]))

N = 16

for t in range(100):
    ax = plt.gca()
    plt.plot(x[N*t*10:N*(t*10+1)], y[N*t*10:N*(t*10+1)], 'ro')
    #circ = plt.Circle((x[i+t], y[i+t]), radius=0.5, color=(0, 1, 0, 0.4))
    #ax.add_patch(circ)
    plt.xlim(0, 8)
    plt.ylim(0, 8)
    plt.savefig('plot/{0:04d}.png'.format(t))
    plt.clf()

